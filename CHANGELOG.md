Steerpath Android Smart SDK Example

# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Explicit Versioning](https://github.com/exadra37-versioning/explicit-versioning).

## [1.1.0] - 2020-02-11

### Changed
- Uses 1.3.6.0
- Removed HighlightActivity (feature not supported anymore)


## [1.0.1] - 2018-12-12

### Changed
- Uses 1.0.0.1

### Added
- More examples: CameraActivity, HighlightActivity, MarkerActivity, NavigationActivity


## [1.0.0] - 2018-12-07

### Added
- Initial test releases

