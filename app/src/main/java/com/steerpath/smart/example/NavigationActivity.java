package com.steerpath.smart.example;

import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;

import com.steerpath.smart.MapResponse;
import com.steerpath.smart.NavigationUserTask;
import com.steerpath.smart.ObjectSource;
import com.steerpath.smart.SmartMapFragment;
import com.steerpath.smart.SmartMapObject;
import com.steerpath.smart.UserTask;
import com.steerpath.smart.listeners.NavigationEventListener;
import com.steerpath.smart.listeners.UserTaskListener;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

/**
 * NavigationActivity demonstrates usage of SmartMapFragment Navigation API.
 *
 * Note: navigation fails because you are not actaully in Steerpath Office where beacons are located.
 * In order to test navigation, use your API and POI data.
 *
 */
public class NavigationActivity extends AppCompatActivity implements NavigationEventListener, UserTaskListener {

    private static final String TAG = NavigationActivity.class.getSimpleName();

    private static final String BUILDING_REF = "431";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        // SDK does not modify screen flags for you
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        if (savedInstanceState == null) {
            SmartMapFragment map = SmartMapFragment.newInstance();
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.map_container, map, "map")
                    .commit();

            map.animateCameraToBuildingRef(BUILDING_REF, event -> Log.i(TAG, "onResponse: " + event));

            // SmartMapFragment is not attached to the Activity at this point,
            // but SmartMapFragment can be used nevertheless.
            //testNavigationFromTheBlueDot(map);
            //testNavigationFromTheBlueDotWithSmartMapObject(map);
            testNavigationFromFixedOrigin(map);
            //testGetMapObjectAndNavigateTo(map);
            //testGetMapObjectAndNavigateThrough(map);
        }
    }

    /**
     * Start navigation from the BlueDot. Find destination with localRef + buidingRef
     * @param map
     */
    private void testNavigationFromTheBlueDot(SmartMapFragment map) {
        map.startUserTask(new NavigationUserTask("Lounge", BUILDING_REF));
    }

    /**
     * Start navigation from the BlueDot. Use pre-defined SmartMapObject as destination.
     * @param map
     */
    private void testNavigationFromTheBlueDotWithSmartMapObject(SmartMapFragment map) {
        SmartMapObject obj = new SmartMapObject(60.220998833, 24.8123412292, 2, "R&D", BUILDING_REF);
        map.startUserTask(new NavigationUserTask(obj));
    }

    /**
     * Start navigation from non-BlueDot location
     * @param map
     */
    private void testNavigationFromFixedOrigin(SmartMapFragment map) {
        NavigationUserTask task = new NavigationUserTask("Lounge", BUILDING_REF);
        task.setOrigin(new SmartMapObject(60.220998833, 24.8123412292, 2, "R&D", BUILDING_REF));
        map.startUserTask(task);
    }

    /**
     * Request SmartMapObject from the map and start navigation
     * @param map
     */
    private void testGetMapObjectAndNavigateTo(SmartMapFragment map) {
        map.getMapObject("Lounge", BUILDING_REF, ObjectSource.STATIC, (mapObject, response) -> {
            Log.i(TAG, "onResponse: " + mapObject + ", " + response);
            if (response == MapResponse.SUCCESS) {
                map.startUserTask(new NavigationUserTask(mapObject));
            }
        });
    }

    /**
     * Navigate through multiple waypoints.
     * @param map
     */
    private void testGetMapObjectAndNavigateThrough(SmartMapFragment map) {
        List<SmartMapObject> waypoints = new ArrayList<>();
        waypoints.add(new SmartMapObject(60.22089814, 24.81234131, 2, "Lounge", BUILDING_REF));
        waypoints.add(new SmartMapObject(60.220998833, 24.8123412292, 2, "R&D", BUILDING_REF)); // last item is the destination
        map.startUserTask(new NavigationUserTask(waypoints));
    }

    @Override
    public void onNavigationDestinationReached() {
        Log.i(TAG, "onNavigationDestinationReached: ");
    }

    @Override
    public void onNavigationFailed(String s) {
        Log.i(TAG, "onNavigationFailed: ");
    }

    @Override
    public void onUserTaskResponse(@NonNull UserTask userTask, String s) {
        Log.i(TAG, "onUserTaskResponse: " + userTask + ", " + s);
    }
}
