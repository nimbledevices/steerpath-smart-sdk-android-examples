package com.steerpath.smart.example;

import android.os.Bundle;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import com.steerpath.smart.MapMode;
import com.steerpath.smart.SmartMapFragment;

public class SearchActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        // SDK does not modify screen flags for you
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        if (savedInstanceState == null) {
            SmartMapFragment map = SmartMapFragment.newInstance();
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.map_container, map, "map")
                    .commit();

            // show persistent SearchBottomSheet. If you later start UserTask,
            // MapMode will be suspended until that UserTask has been stopped.
            map.setMapMode(MapMode.SEARCH);
        }
    }
}
