package com.steerpath.smart.example;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;
import com.steerpath.ux.MenuItem;
import com.steerpath.ux.MenuSectionFragment;
import com.steerpath.ux.MenuViewPagerAdapter;

import java.util.ArrayList;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;

public class MainActivity extends AppCompatActivity implements MenuSectionFragment.DataPassListener {

    private ViewPager viewPager;
    private TabLayout tabLayout;
    private MenuViewPagerAdapter adapter;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);

        drawerLayout = findViewById(R.id.drawer_layout);
        setDrawerLayout(drawerLayout);

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(item -> {

            switch (item.getItemId()) {
                case R.id.info:
                    Toast.makeText(getApplicationContext(), "Info clicked", Toast.LENGTH_SHORT);
                    break;
            }

            item.setChecked(true);
            drawerLayout.closeDrawers();

            return false;
        });

        viewPager = findViewById(R.id.view_pager);
        setUpViewPager(viewPager);

        tabLayout = findViewById(R.id.tab_layout);
        setTabLayout(tabLayout);
    }

    @Override
    public void onDataPass(MenuItem menuItem) {
        switch (menuItem.getName()) {
            default:
                launchActivity(menuItem.getActivity());
        }
    }

    private void launchActivity(Class<?> activityClass) {
        Intent intent = new Intent(this, activityClass);
        startActivity(intent);
    }

    // Set fragments for tablelayout and populate lists for each fragment, set up ViewPager
    private void setUpViewPager(ViewPager viewPager) {

        adapter = new MenuViewPagerAdapter(getSupportFragmentManager());

        ArrayList<MenuItem> options = new ArrayList<>();
        options.add(new MenuItem("Basic Map", "A \" Drop-in\" map. Attach SmartMapFragment to Activity.", BasicMapActivity.class));
        options.add(new MenuItem("Camera", "Move camera to POI or building", CameraActivity.class));
        options.add(new MenuItem("Markers", "Add or remove Markers on the map", MarkerActivity.class));
        options.add(new MenuItem("Navigation", "Start navigation", NavigationActivity.class));
        options.add(new MenuItem("Search", "Search MapMode", SearchActivity.class));
        options.add(new MenuItem("Geofencing", "Receive Geofence events from SmartGeofenceManager", GeofencingActivity.class));
        adapter.addFrag(MenuSectionFragment.newInstance(options), "Examples");

        viewPager.setAdapter(adapter);
        MenuSectionFragment fragment;

        //Set DataPassListener for each Fragment
        for (int i = 0; i < adapter.getCount(); i++) {
            fragment = adapter.getItem(i);
            fragment.setDataPassListener(this);
        }
    }

    // TODO: no content here, yet?
    private void setDrawerLayout(final DrawerLayout drawer) {
        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {

            }

            @Override
            public void onDrawerClosed(View drawerView) {
                navigationView.getMenu().getItem(0).setChecked(false);
            }


            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });

    }

    // TODO: no content here, yet?
    private void setTabLayout(TabLayout tabLayout) {
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
}
