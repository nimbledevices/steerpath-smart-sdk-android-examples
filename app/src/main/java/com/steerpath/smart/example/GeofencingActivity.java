package com.steerpath.smart.example;

import android.os.Bundle;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.steerpath.smart.SmartGeofenceManager;
import com.steerpath.smart.SmartMapFragment;
import com.steerpath.smart.listeners.FenceEventListener;

/**
 * SmartSDK will automatically configure your geofences. You just need to listen for the updates.
 * This class demonstrates two use cases:
 *
 * 1. Use with SmartMapFragment: let this Activity implement FenceEventListener
 * 2. Use without SmartMapFragment: see {@link GeofencingActivity#addListener()}
 *
 * In order to test this, you need actual beacons and Steerpath support needs to add geofences to your map data.
 */
public class GeofencingActivity extends AppCompatActivity /*1: use case */ implements FenceEventListener {

    protected static final String TAG = GeofencingActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        // SDK does not modify screen flags for you
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        if (savedInstanceState == null) {
            SmartMapFragment map = SmartMapFragment.newInstance();
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.map_container, map, "map")
                    .commit();
        }
    }

    @Override
    public void onGeofenceEnter(String localRef, String buildingRef) {
        Toast.makeText(GeofencingActivity.this, "Geofence " + localRef + "/" + buildingRef + " entered", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onGeofenceExit(String localRef, String buildingRef) {
        Toast.makeText(GeofencingActivity.this, "Geofence " + localRef + "/" + buildingRef + " exited", Toast.LENGTH_LONG).show();
    }

    /**
     * 2: use case. You can also listen for the events without the SmartMapFragment.
     */
    private void addListener() {
        SmartGeofenceManager.addListener(new FenceEventListener() {
            @Override
            public void onGeofenceEnter(String localRef, String buildingRef) {
                Toast.makeText(GeofencingActivity.this, "Geofence " + localRef + "/" + buildingRef + " entered", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onGeofenceExit(String localRef, String buildingRef) {
                Toast.makeText(GeofencingActivity.this, "Geofence " + localRef + "/" + buildingRef + " exited", Toast.LENGTH_LONG).show();
            }
        });
    }
}
