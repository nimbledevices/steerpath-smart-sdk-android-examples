package com.steerpath.smart.example;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.WindowManager;

import com.steerpath.smart.MapMode;
import com.steerpath.smart.SmartMapFragment;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class BasicMapActivity extends AppCompatActivity {

    protected static final String TAG = BasicMapActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        // SDK does not modify screen flags for you
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        if (savedInstanceState == null) {
            SmartMapFragment map = SmartMapFragment.newInstance();
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.map_container, map, "map")
                    .commit();

            map.setMapMode(MapMode.MAP_ONLY);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_test, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_test:
                onTestButtonPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void onTestButtonPressed() {
        // nothing to do in the base class
    }

    /**
     * Having SmartMapFragment as member variable is somewhat hazardous. Depending on where
     * variable is assigned, it may or may not be valid after screen orientation change.
     * It's much more safer to get SmartMapFragment from FragmentManager and do the null check.
     * @return
     */
    @Nullable
    private SmartMapFragment getMap() {
        return (SmartMapFragment) getSupportFragmentManager().findFragmentByTag("map");
    }
}
