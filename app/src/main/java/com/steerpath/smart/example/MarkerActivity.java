package com.steerpath.smart.example;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.steerpath.smart.Layout;
import com.steerpath.smart.MapResponse;
import com.steerpath.smart.ObjectSource;
import com.steerpath.smart.SmartMapFragment;
import com.steerpath.smart.SmartMapObject;

import java.util.ArrayList;
import java.util.List;

/**
 * MarkerActivity demonstrates usage of SmartMapFragment Marker API.
 */
public class MarkerActivity extends AppCompatActivity {

    private static final String TAG = "example";

    private static final String BUILDING_REF = "431";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        // SDK does not modify screen flags for you
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        if (savedInstanceState == null) {
            SmartMapFragment map = SmartMapFragment.newInstance();
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.map_container, map, "map")
                    .commit();

            // SmartMapFragment is not attached to the Activity at this point,
            // but SmartMapFragment can be used nevertheless.
            testApi(map);
            testGetMapObjectAndAddMarkers(map);
            //testAddMarkers(map);
        }
    }

    /**
     * Set arbitrary markers
     * @param map
     */
    private void testApi(SmartMapFragment map) {
        SmartMapObject target = new SmartMapObject(60.22089814, 24.81234131, 2, "Lounge", BUILDING_REF);
        SmartMapObject target2 = new SmartMapObject(60.220998833, 24.8123412292, 2, "R&D", BUILDING_REF);

        map.addMarker(target);
        map.addMarker(target2);
    }

    /**
     * Get SmartMapObject from the map and set marker
     * @param map
     */
    private void testGetMapObjectAndAddMarkers(SmartMapFragment map) {
        map.getMapObject("Private Office 02", "67", ObjectSource.STATIC, (mapObject, response) -> {
            Log.i(TAG, "onResponse: " + mapObject + ", " + response);
            if (response == MapResponse.SUCCESS) {
                map.addMarker(mapObject);
            }
        });
    }

    private void testAddMarkers(SmartMapFragment map) {

        // add custom image
        map.addIconImage("testIcon", R.drawable.ic_pin_drop);

        List<SmartMapObject> objectList = new ArrayList<>();
        SmartMapObject o1 = new SmartMapObject(60.22089814, 24.81234131, 2, "Lounge", BUILDING_REF);
        o1.setTitle("Hello Lounge");
        objectList.add(o1);

        SmartMapObject o2 = new SmartMapObject(60.220998833, 24.8123412292, 2, "R&D", BUILDING_REF);
        o2.setTitle("Hello R&D");
        objectList.add(o2);

        map.addMarkers(objectList, Layout.BOTTOM, "testIcon", "#ff0000", "#000000");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_test, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_test:
                onTestButtonPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Having SmartMapFragment as member variable is somewhat hazardous. Depending on where
     * variable is assigned, it may or may not be valid after screen orientation change.
     * It's much more safer to get SmartMapFragment from FragmentManager and do the null check.
     * @return
     */
    @Nullable
    private SmartMapFragment getMap() {
        return (SmartMapFragment) getSupportFragmentManager().findFragmentByTag("map");
    }

    protected void onTestButtonPressed() {
        if (getMap() != null) {
            getMap().removeAllMarkers();
        }
    }
}
