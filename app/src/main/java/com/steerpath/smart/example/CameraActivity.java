package com.steerpath.smart.example;

import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import com.steerpath.smart.SmartMapFragment;

/**
 * CameraActivity demonstrates usage of SmartMapFragment Camera API.
 */
public class CameraActivity extends AppCompatActivity {

    private static final String TAG = "example";

    private static final String BUILDING_REF = "431";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        // SDK does not modify screen flags for you
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        if (savedInstanceState == null) {
            SmartMapFragment map = SmartMapFragment.newInstance();
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.map_container, map, "map")
                    .commit();

            // SmartMapFragment is not attached to the Activity at this point,
            // but SmartMapFragment can be used nevertheless.
            testApi(map);
        }
    }

    private void testApi(SmartMapFragment map) {
        // setCamera/animateCamera variants. Feel free to explore!
        //map.setCamera(60.220842, 24.812215, 21);
        //map.setCamera(60.220842, 24.812215, 19, -16.84271861249931, 45);
        //map.setCamera(60.220842, 24.812215, 19, -16.84271861249931, 45, 2, BUILDINF_REF);
        //map.setCameraToObject("Lounge", BUILDING_REF, 20, (event) -> Log.i(TAG, "onResponse: setCameraToObject:  " + event));
        //map.setCameraToObject("Lounge", BUILDING_REF, 20, null);
        //map.setCameraToBuildingRef(BUILDING_REF, event -> Log.i(TAG, "onResponse: " + event));

        //map.animateCamera(60.220842, 24.812215, 21);
        //map.animateCamera(60.220842, 24.812215, 19, -16.84271861249931, 45);
        //map.animateCamera(60.220842, 24.812215, 19, -16.84271861249931, 45, 2, BUILDINF_REF);
        //map.animateCameraToObject("Lounge", BUILDING_REF, 20, (event) -> Log.i(TAG, "onResponse: setCamera:  " + event));
        //map.animateCameraToObject("Lounge", BUILDING_REF, 20, null);
        map.animateCameraToBuildingRef(BUILDING_REF, event -> Log.i(TAG, "onResponse: " + event));
    }
}