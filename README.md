# Copyright Steerpath Ltd. 2020. All rights reserved #

Steerpath Android SmartSDK Examples.

## What is this repository for?

* Code examples how to use Steerpath Smart SDK for Android
* Version 1.1.0

## SDK Configuration

### API Key

By default, example uses public Steerpath Office API key. Replace it with your own API key.

Look for:

```
com.steerpath.smart.example.ExampleApplication
```

Start the SDK with your API key:

```
SmartSDK.getInstance().start(this, <apiKey_here>);
```                

### POI List

Some SmartSDK methods require map / venue specific POI information as input parameter. For example, SmartMapFragment.animateCameraToObject() requires *localRef* and *buildingRef*. Based on your contract with Steerpath, you are provided this POI information separately. 


### Offline Data (Optional)

Application can be configured to use offline data package called sp\_offline\_data.sff. Based on your contract with Steerpath, you are provided this package separately. Offline data is not hard requirement for the SDK to operate.

Steps to enable:

* Enable following flag in app's Manifest.xml
```
<meta-data android:name="SP_OFFLINE_DATA" android:value="sp_offline_data.sff" />
```

* Copy sp\_offline\_data.sff file in your projects /assets -folder

### SDK logging (Optional)

Use following flag in app's Manifest:
```
<meta-data android:name="SDK_LOGGING_ENABLED" android:value="true" />
```

When enabled, an additional overlay button will appear on the map view. To filter logs in terminal, use:

```
adb logcat *:S Monitor:V
```

## Contact

* support@steerpath.com